import axios from 'axios';
const state = {
    username: null,
    acl: null,
};
const getters = {
    isAuthenticated: state => !!state.username,    
    StateUser: state => state.username,
};
const actions = {
    async LogIn({commit}, User){
        console.log(User)
    },
    async LogOut({commit}){
        let user = null
        commit('logout', user)
    }
};
const mutations = {
    setUser(state, username, acl){
        state.username = username
        state.acl = acl
    },
    LogOut(state){
        state.username = null
        state.acl = null
    }
};
export default {
  state,
  getters,
  actions,
  mutations
};
