import Vue from "vue";
import axios from "axios";
import Vuex from "vuex";
import createdPersistedState from "vuex-persistedstate";
import auth from './modules/auth';
// import 'es6-promise/auto';
import layout from './modules/layout'
import menu from './modules/menu'
Vue.use(Vuex);

// export default new Vuex.Store({
//   modules: {
//     auth
//   },
//   plugins: [createdPersistedState()]
// });

export const store = new Vuex.Store({
    state: {
      username: null,
      acl: null,
    },
    mutations: {
      setUser(state, username){
        state.username = username
      },
      setAcl(state, acl){
        state.acl = acl
      },
      LogOut(state){
          state.username = null
          state.acl = null
      }
    },
    actions: {
      async LogIn({commit}, User){
        let self = this;
        let responses;
        await axios.post('http://backend.pesonabarokah.com/api/auth/login',User)
        .then(function(response){
          self.responses = response.data
        })
        console.log(this.responses.barokahuseracl)
        commit('setUser', this.responses.username)
        commit('setAcl', this.responses.barokahuseracl)
      },
      async LogOut({commit}){
          let username = null
          let barokahuseracl = null
          commit('LogOut', username)
      }
    },
    getters: {
      isAuthenticated: state => !!state.username,    
      StateUser: state => state.username,
      StateAcl: state => state.acl,
    },
    modules: {
      layout,
      menu
    },
    plugins: [createdPersistedState()]
});

