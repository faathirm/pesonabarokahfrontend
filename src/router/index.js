import Vue from 'vue'
import Router from "vue-router";

import Body from '../components/body'

import SamplePage from '../pages/sample_page'

import StokBarang from '../pages/inventaris/stok_barang/list'
import StokBarangTambah from '../pages/inventaris/stok_barang/tambah'
import StokBarangUbah from '../pages/inventaris/stok_barang/ubah'

import Inventarismasuk from '../pages/inventaris/inventaris_masuk/list'
import InventarismasukTambah from '../pages/inventaris/inventaris_masuk/tambah'
import InventarismasukTambahManual from '../pages/inventaris/inventaris_masuk/tambahManual'
import InventarismasukUbah from '../pages/inventaris/inventaris_masuk/ubah'

import Inventariskeluar from '../pages/inventaris/inventaris_keluar/list'
import InventariskeluarTambah from '../pages/inventaris/inventaris_keluar/tambah'
import InventariskeluarUbah from '../pages/inventaris/inventaris_keluar/ubah'

import Inventarisoperasional from '../pages/inventaris/inventaris_operasional/list'
import InventarisoperasionalTambah from '../pages/inventaris/inventaris_operasional/tambah'
import InventarisoperasionalUbah from '../pages/inventaris/inventaris_operasional/ubah'

import Pembelipembelian from '../pages/pembeli/pembelian/list'
import PembelipembelianTambahKavling from '../pages/pembeli/pembelian/tambahKavling'
import PembelipembelianTambahRumah from '../pages/pembeli/pembelian/tambahBangunan'
import PembelipembelianUbahKavling from '../pages/pembeli/pembelian/ubahKavling'
import PembelipembelianUbahRumah from '../pages/pembeli/pembelian/ubahRumah'

import Pembelipengembalian from '../pages/pembeli/pengembalian/list'
import PembelipengembalianTambah from '../pages/pembeli/pengembalian/tambah'
import PembelipengembalianUbah from '../pages/pembeli/pengembalian/ubah'

import Pembayaran from '../pages/pembayaran/list'
import PembayaranTambah from '../pages/pembayaran/tambah'
import PembayaranUbah from '../pages/pembayaran/ubah'

import Keluhan from '../pages/keluhan/list'
import KeluhanTambah from '../pages/keluhan/tambah'
import KeluhanUbah from '../pages/keluhan/ubah'

import Pengguna from '../pages/pengguna/list'
import PenggunaTambah from '../pages/pengguna/tambah'
import PenggunaUbah from '../pages/pengguna/ubah'

import PenggajianKaryawan from '../pages/penggajian/karyawan/list'
import PenggajianKaryawanTambah from '../pages/penggajian/karyawan/tambah'
import PenggajianKaryawanUbah from '../pages/penggajian/karyawan/ubah'

import PenggajianTukang from '../pages/penggajian/tukang/list'
import PenggajianTukangTambah from '../pages/penggajian/tukang/tambah'
import PenggajianTukangUbah from '../pages/penggajian/tukang/ubah'

import Dashboard from '../pages/dashboard/list'

import Auth from '../auth/auth';
import login from '../auth/login';

import store from "../store/index";

// component

Vue.use(Router)

const routes = [
  { path: '', redirect: { name: 'dashboard' }},
  {
    path: '/inventaris',
    component: Body,
    children: [
    {
      path: 'stokbarang',
      name: 'stokbarang',
      component: StokBarang,
      meta: {
        title: 'Inventaris | Stok Barang',
        requiresAuth: true,
        access: 'inventaris-stok-view'
      }
    },
    {
      path: 'stokbarang/tambah',
      name: 'stokbarang/tambah',
      component: StokBarangTambah,
      meta: {
        title: 'Inventaris | Tambah Stok Barang',
        requiresAuth: true
      }
    },
    {
      path: 'stokbarang/ubah',
      name: 'stokbarang/ubah',
      component: StokBarangUbah,
      meta: {
        title: 'Inventaris | Ubah Stok Barang',
        requiresAuth: true
      }
    },
    {
      path: 'masuk',
      name: 'masuk',
      component: Inventarismasuk,
      meta: {
        title: 'Inventaris | Masuk',
        requiresAuth: true
      }
    },
    {
      path: 'masuk/tambah',
      name: 'masuk/tambah',
      component: InventarismasukTambah,
      meta: {
        title: 'Inventaris | Tambah Masuk',
        requiresAuth: true
      }
    },
    {
      path: 'masuk/tambah/manual',
      name: 'masuk/tambahManual',
      component: InventarismasukTambahManual,
      meta: {
        title: 'Inventaris | Tambah Manual Masuk',
        requiresAuth: true
      }
    },
    {
    path: 'masuk/ubah',
    name: 'masuk/ubah',
    component: InventarismasukUbah,
    meta: {
      title: 'Inventaris | Ubah',
      requiresAuth: true
    }
    },
    {
      path: 'keluar',
      name: 'keluar',
      component: Inventariskeluar,
      meta: {
        title: 'Inventaris | Keluar',
        requiresAuth: true
      }
    },
    {
      path: 'keluar/tambah',
      name: 'keluar/tambah',
      component: InventariskeluarTambah,
      meta: {
        title: 'Inventaris | Tambah Keluar',
        requiresAuth: true
      }
    },
    {
    path: 'keluar/ubah',
    name: 'keluar/ubah',
    component: InventariskeluarUbah,
    meta: {
      title: 'Inventaris | Ubah Keluar',
      requiresAuth: true
    }
    },
    {
      path: 'operasional',
      name: 'operasional',
      component: Inventarisoperasional,
      meta: {
        title: 'Inventaris | Operasional',
        requiresAuth: true
      }
    },
    {
      path: 'operasional/tambah',
      name: 'operasional/tambah',
      component: InventarisoperasionalTambah,
      meta: {
        title: 'Inventaris | Tambah Operasional',
        requiresAuth: true
      }
    },
    {
    path: 'operasional/ubah',
    name: 'operasional/ubah',
    component: InventarisoperasionalUbah,
    meta: {
      title: 'Inventaris | Ubah Operasional',
      requiresAuth: true
    }
    },
    ]
  },
  {
    path: '/pembeli',
    component: Body,
    children: [
      {
        path: 'pembelian',
        name: 'pembelian',
        component: Pembelipembelian,
        meta: {
          title: 'Pembeli | Pembelian',
          requiresAuth: true
        }
      },
      {
        path: 'pembelian/tambah/kavling',
        name: 'pembelian/tambah/kavling',
        component: PembelipembelianTambahKavling,
        meta: {
          title: 'Pembeli | Tambah Pembelian Kavling',
          requiresAuth: true
        }

      },
      {
        path: 'pembelian/tambah/rumah',
        name: 'pembelian/tambah/rumah',
        component: PembelipembelianTambahRumah,
        meta: {
          title: 'Pembeli | Tambah Pembelian Rumah',
          requiresAuth: true
        }

      },
      {
      path: 'pembelian/ubah/kavling',
      name: 'pembelian/ubah/kavling',
      component: PembelipembelianUbahKavling,
      meta: {
        title: 'Pembeli | Ubah Pembelian Kavling',
        requiresAuth: true
      }
      },
      {
        path: 'pembelian/ubah/rumah',
        name: 'pembelian/ubah/rumah',
        component: PembelipembelianUbahRumah,
        meta: {
          title: 'Pembeli | Ubah Pembelian Rumah',
          requiresAuth: true
        }
        },
        {
          path: 'pengembalian',
          name: 'pengembalian',
          component: Pembelipengembalian,
          meta: {
            title: 'Pembeli | Pengembalian',
            requiresAuth: true
          }
        },
        {
          path: 'pengembalian/tambah',
          name: 'pengembalian/tambah',
          component: PembelipengembalianTambah,
          meta: {
            title: 'Pembeli | Tambah Pengembalian',
            requiresAuth: true
          }
        },
        {
        path: 'pengembalian/ubah',
        name: 'pengembalian/ubah',
        component: PembelipengembalianUbah,
        meta: {
          title: 'Pembeli | Ubah Pengembalian',
          requiresAuth: true
        }
        },
    ]
  },
  {
    path: '/app',
    component: Body,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
          title: 'Dashboard',
          requiresAuth: true,
          access: 'dashboard-view'
        }
      },  
    {
      path: 'pembayaran',
      name: 'pembayaran',
      component: Pembayaran,
      meta: {
        title: 'Pembayaran',
        requiresAuth: true
      }
    },
    {
      path: 'pembayaran/tambah',
      name: 'pembayaran/tambah',
      component: PembayaranTambah,
      meta: {
        title: 'Pembayaran | Tambah',
        requiresAuth: true
      }
    },
    {
      path: 'pembayaran/ubah',
      name: 'pembayaran/ubah',
      component: PembayaranUbah,
      meta: {
        title: 'Pembayaran | Ubah',
        requiresAuth: true
      }
    },
    {
      path: 'keluhan',
      name: 'keluhan',
      component: Keluhan,
      meta: {
        title: 'Keluhan',
        requiresAuth: true
      }
    },
    {
      path: 'keluhan/tambah',
      name: 'keluhan/tambah',
      component: KeluhanTambah,
      meta: {
        title: 'Keluhan | Tambah',
        requiresAuth: true
      }
    },
    {
      path: 'keluhan/ubah',
      name: 'keluhan/ubah',
      component: KeluhanUbah,
      meta: {
        title: 'Keluhan | Ubah',
        requiresAuth: true
      }
    },
    {
      path: 'pengguna',
      name: 'pengguna',
      component: Pengguna,
      meta: {
        title: 'Pengguna',
        requiresAuth: true
      }
    },
    {
      path: 'pengguna/tambah',
      name: 'pengguna/tambah',
      component: PenggunaTambah,
      meta: {
        title: 'Pengguna | Tambah',
        requiresAuth: true
      }
    },
    {
      path: 'pengguna/ubah',
      name: 'pengguna/ubah',
      component: PenggunaUbah,
      meta: {
        title: 'Pengguna | Ubah',
        requiresAuth: true
      }
    }
  ]},
  {
    path: '/penggajian',
    component: Body,
    children: [
    {
      path: 'karyawan',
      name: 'karyawan',
      component: PenggajianKaryawan,
      meta: {
        title: 'Penggajian | Karyawan',
        requiresAuth: true
      }
    },
    {
      path: 'karyawan/tambah',
      name: 'karyawan/tambah',
      component: PenggajianKaryawanTambah,
      meta: {
        title: 'Penggajian | Tambah Karyawan',
        requiresAuth: true
      }
    },
    {
      path: 'karyawan/ubah',
      name: 'karyawan/ubah',
      component: PenggajianKaryawanUbah,
      meta: {
        title: 'Penggajian | Ubah Karyawan',
        requiresAuth: true
      }
    },
    {
      path: 'tukang',
      name: 'tukang',
      component: PenggajianTukang,
      meta: {
        title: 'Penggajian | Tukang',
        requiresAuth: true
      }
    },
    {
      path: 'tukang/tambah',
      name: 'tukang/tambah',
      component: PenggajianTukangTambah,
      meta: {
        title: 'Penggajian | Tambah Tukang',
        requiresAuth: true
      }
    },
    {
      path: 'tukang/ubah',
      name: 'tukang/ubah',
      component: PenggajianTukangUbah,
      meta: {
        title: 'Penggajian | Ubah Tukang',
        requiresAuth: true
      }
    },
  ]},
  {
    path: '/auth',
    component: Auth,
    children: [
    {
      path: 'login',
      name: 'login',
      component: login,
      meta: {
        title: ' login ',
        guest: true
      }
    },
    ]
  },
];

const router = new Router({
  routes,
  base: '/',
  mode: 'history',
  linkActiveClass: "active",
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (router.app.$store.getters.isAuthenticated) {
      next();
      return;
    }
    next("/auth/login");
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    if (router.app.$store.getters.isAuthenticated) {
      next("/app/dashboard");
      return;
    }
    next();
  } else {
    next();
  }
});


export default router
