import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import router from './router'
import Breadcrumbs from './components/bread_crumbs'
import { store } from './store';
import VueFeather from 'vue-feather';
import VueAxios from 'vue-axios'
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueApexCharts from 'vue-apexcharts'
import _ from 'lodash'

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

import PxCard  from './components/Pxcard.vue'
Vue.component(PxCard.name, PxCard)

// Import Theme scss
import './assets/scss/app.scss'
// import { _ } from 'core-js'

axios.interceptors.response.use(undefined, function(error) {
  if (error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      store.dispatch("LogOut");
      return router.push("/auth/login");
    }
  }
});


Vue.use(VueFeather);
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueAxios, axios)
Vue.use(VueSweetalert2)
Vue.component('Breadcrumbs', Breadcrumbs)
Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)

Vue.mixin({
  methods: {
    checkAcl(acl){
      const a = _.find(this.$store.getters.StateAcl, {'access':acl})
      if(a){
        return true;
      }else{
        return false;
      }
    }
  }
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')